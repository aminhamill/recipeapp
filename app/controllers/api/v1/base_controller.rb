class Api::V1::BaseController < ApplicationController

  def authenticate_request_user
    @current_user = AuthorizeApiRequestUser.call(request.headers).result
    render json: { error: 'Not Authorized. please signin' }, status: 401 unless @current_user
  end

end
