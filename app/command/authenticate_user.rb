class AuthenticateUser
  prepend SimpleCommand

  def initialize(phone, otp)
    @phone = phone
    @otp = otp
  end

  def call
    encode(user_id: user.id) if user
  end

  attr_accessor :phone, :otp


  def user
    user = User.find_by_phone(phone)
    return user if (user && user.otp == otp) and user.active?
    errors.add :user_authentication, 'invalid credentials'
    nil
  end

  def encode(payload, exp = 12.month.from_now)
    payload[:exp] = exp.to_i
    JWT.encode(payload, Rails.application.secrets.secret_key_base)
  end

  def decode(token)
    body = JWT.decode(token, Rails.application.secrets.secret_key_base)[0]
    HashWithIndifferentAccess.new body
  rescue
    nil
  end

end
