class Category < ApplicationRecord
  mount_uploader :image, CategoryUploader
  belongs_to :category, optional: true
  has_many :category_foods
  has_many :foods, through: :category_foods

  enum status: {
    active: 1,
    inactive: 2
  }
end
