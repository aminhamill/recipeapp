class Ingredient < ApplicationRecord
  belongs_to :ingredient_category
  has_many :food_ingredients
  has_many :foods, through: :food_ingredients

  mount_uploader :image, IngredientUploader
end
