class Food < ApplicationRecord
  mount_uploader :image, FoodUploader
  has_many :category_foods
  has_many :categories, through: :category_foods

  has_many :food_ingredients
  has_many :ingredients, through: :food_ingredients

  enum status: {
    active: 1,
    inactive: 2
  }
end
